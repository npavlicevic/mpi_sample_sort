#ifndef SAMPLE_SORT_H
#define SAMPLE_SORT_H
#include "mpi_vector.h"
void mpi_sample_splitters(float splitter_v[], float local_v[], int n, int p);
void mpi_sample_splitters_move(float _splitter_v[], float splitter_v[], int p);
void mpi_sample_buckets(float bucket_v[], float splitter_v[], float local_v[], int n, int p);
void mpi_sample_buckets_move(float _bucket_v[], float bucket_v[], int n, int p);
void mpi_sample_output_move(float _out_v[], float out_v[], int n, int p);
#endif
