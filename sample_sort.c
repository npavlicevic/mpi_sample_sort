#include "sample_sort.h"
void mpi_sample_splitters(float splitter_v[], float local_v[], int n, int p) {
  int splitters = p - 1, i;
  for(i = 0; i < splitters; i++) {
    splitter_v[i] = local_v[n / (p * p) * (i + 1)];
  }
}
void mpi_sample_splitters_move(float _splitter_v[], float splitter_v[], int p) {
  int i;
  for(i = 0; i < p - 1; i++) {
    _splitter_v[i] = splitter_v[(p - 1) * (i + 1)];
  }
  // choose splitters again
}
void mpi_sample_buckets(float bucket_v[], float splitter_v[], float local_v[], int n, int p) {
  int i, j = 0, k = 1;
  for(i = 0; i < n; i++) {
    if(j < (p - 1)) {
      if(local_v[i] < splitter_v[j]) {
        bucket_v[((n + 1) * j) + k++] = local_v[i];
      } else {
        bucket_v[(n + 1) * j] = k - 1;
        k = 1;
        j++;
        i--;
      }
    } else {
      bucket_v[((n + 1) * j) + k++] = local_v[i];
    }
  }
  bucket_v[(n + 1) * j] = k - 1;
}
void mpi_sample_buckets_move(float _bucket_v[], float bucket_v[], int n, int p) {
  int i, j, k, count = 1;
  for(j = 0; j < p; j++) {
    k = 1;
    for(i = 0; i < bucket_v[(n / p + 1) * j]; i++) {
      _bucket_v[count++] = bucket_v[(n / p + 1) * j + k++];
    }
  }
  _bucket_v[0] = count - 1;
}
void mpi_sample_output_move(float _out_v[], float out_v[], int n, int p) {
  int i, j, k, count = 0;
  for(j = 0; j < p; j++) {
    k = 1;
    for(i = 0; i < out_v[(2 * n / p) * j]; i++) {
      _out_v[count++] = out_v[(2 * n / p) * j + k++];
    }
  }
}
