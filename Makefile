#
# Makefile 
# sample sort
#

CC=mpicc
LIBS=-lmpi -lmpivector
FILES=sample_sort.c main.c
FILES_SAMPLE=main_sample.c
REMOVE=main main_sample

all: main sample

main: ${FILES}
	${CC} $^ -o main ${LIBS}

sample: ${FILES_SAMPLE}
	${CC} $^ -o main_sample ${LIBS}

clean: ${REMOVE}
	rm $^
