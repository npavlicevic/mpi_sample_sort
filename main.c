#include "sample_sort.h"
main(int argc, char **argv) {
  int my_rank;
  int p;
  int n;
  // n number of elements to sort globally
  int n_bar;
  int n_to_sort;
  int i;
  int j;
  int k;
  float local_v[MAX_LOCAL_ORDER];
  // all items
  float _local_v[MAX_LOCAL_ORDER];
  // local items
  float splitter_v[MAX_LOCAL_ORDER];
  float splitter_all_v[MAX_LOCAL_ORDER];
  float bucket_v[MAX_LOCAL_ORDER];
  // number of items + number of processors
  float bucket_other_v[MAX_LOCAL_ORDER];
  float _bucket_v[MAX_LOCAL_ORDER];
  float out_v[MAX_LOCAL_ORDER];
  float _out_v[MAX_LOCAL_ORDER];
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  fscanf(stdin, "%d", &n);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  n_bar = n / p;
  if(!my_rank) {
    mpi_random_vector(local_v, n);
    mpi_print_vector_local(local_v, n);
  }
  mpi_scatter_vector(_local_v, local_v, n_bar);
  // todo change this to mpi_print_local_vector
  qsort(_local_v, n_bar, sizeof(float), mpi_float_compare);
  // do local sort
  mpi_sample_splitters(splitter_v, _local_v, n, p);
  // choose local splitters (number of processors - 1)
  mpi_gather_vector(splitter_all_v, splitter_v, p - 1);
  // gather all splitters at root
  if(!my_rank) {
    qsort(splitter_all_v, p * (p - 1), sizeof(float), mpi_float_compare);
    // sort splitters
    mpi_sample_splitters_move(splitter_v, splitter_all_v, p);
  }
  mpi_broadcast_vector(splitter_v, p - 1);
  mpi_sample_buckets(bucket_v, splitter_v, _local_v, n_bar, p);
  mpi_all_to_all_vector(bucket_other_v, bucket_v, n_bar + 1);
  mpi_sample_buckets_move(_bucket_v, bucket_other_v, n, p);
  n_to_sort = _bucket_v[0];
  qsort(&_bucket_v[1], n_to_sort, sizeof(float), mpi_float_compare);
  mpi_gather_vector(out_v, _bucket_v, 2 * n_bar);
  if(!my_rank) {
    mpi_sample_output_move(_out_v, out_v, n, p);
    mpi_print_vector_local(_out_v, n);
  }
  MPI_Finalize();
}
